# ABM Personas - Aplicación Frontend React

### Introducción:

Este proyecto te permite gestionar personas con una aplicación web completa utilizando React.

## Requisitos:

Node.js v16.20.0 o superior
npm o yarn
## Levantamiento del proyecto:

### Opción 1: Levantamiento con Docker:

Levanta el frontend:
```
docker-compose up -d frontend
```
Opción 2: Levantamiento tradicional:

Instalar dependencias:
Ejecuta 
```
npm install 
```
o 
```
yarn install
```
en la carpeta del proyecto.

Iniciar el servidor:
Ejecuta 
```
npm start 
```
o 
```
yarn start
```

## Acceso a la aplicación:

Frontend: 
```
http://localhost:3000
```

## Solución de problemas:

Si el frontend no se levanta con docker-compose up -d, puedes intentar la opción 2 de levantamiento tradicional.
Si tienes problemas al ejecutar el frontend, asegúrate de tener instalado Node.js v16.20.0 o superior.
Consulta la sección "Contacto" si necesitas ayuda.
Contacto: Ronald Alfonso, raafolfonso@gmail.com

Si tienes preguntas o necesitas ayuda, puedes contactarnos a través de tu canal de contacto preferido: inserta tu canal de contacto.

## Agradecimientos:

A la comunidad de React por su framework frontend flexible y potente.
Esperamos que este proyecto te sea útil.
