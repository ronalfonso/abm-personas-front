export interface ResponseRequest {
    data: any;
    status: number;
    statusText: string;
    error: boolean;
    message: string;
}