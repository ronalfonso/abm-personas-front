import {useContext, useEffect, useState} from "react";
import {
    Box,
    Button,
    Dialog, DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel, MenuItem,
    Select,
    TextField,
    Typography
} from "@mui/material"
import {GeneralContext} from "../../../contexts";
import {capitalizeLabel} from "../../utils/handle-lables";
import * as Yup from "yup";
import {useFormik} from "formik";
import {useAppDispatch, useAppSelector} from "../../../store/store";
import {ITipoDocumentos} from "../../../store/personas/interfaces/TipoDocumentos";
import {startCreatePersona, startDeletePersona, startUpdatePersona} from "../../../store/personas";
import moment from "moment";

export const PersonaDialog = () => {
    const {
        openPersonaDialog,
        setOpenPersonaDialog,
        setRecharge,
        idItem,
        setIdItem,
        itemEdit,
    } = useContext<any>(GeneralContext);
    const {tiposDocumento} = useAppSelector((state) => state.personas)
    const dispatch = useAppDispatch();
    const [title, setTitle] = useState('')
    const [types, setTypes] = useState<ITipoDocumentos[]>([])

    const yupSchema = Yup.object({
        perNombre: Yup.string()
            .required("El nombre es obligatorio")
            .min(4, "El nombre debe tener al menos 4 caracteres")
            .max(25, "El nombre no debe tener más de 25 caracteres"),
        perApellido: Yup.string()
            .required("El apellido es obligatorio")
            .min(4, "El apellido debe tener al menos 4 caracteres")
            .max(25, "El apellido no debe tener más de 25 caracteres"),
        perNumeroDocumento: Yup.number()
            .required("El número de documento es obligatorio")
            .min(5, "El número de documento debe tener al menos 5 dígitos"),
        perTipoDocumento: Yup.string().required("El tipo de documento es obligatorio"),
        perFechaNacimiento: Yup.date()
            .required("La fecha de nacimiento es obligatoria")
    });

    const {values,
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isValid,
        resetForm,
        setFieldValue} = useFormik({
        initialValues: {
            perNombre: "",
            perApellido: "",
            perNumeroDocumento: "",
            perTipoDocumento: "",
            perFechaNacimiento: '',
        },
        validationSchema: yupSchema,
        onSubmit: (values) => {
            if (openPersonaDialog.action === 'crear') {
                dispatch(startCreatePersona(values))
                resetForm();
            } else {
                dispatch(startUpdatePersona(values, itemEdit.id))
            }
            handleClose();
            setRecharge(true);
        },
    });

    const handleClose = () => {
        setOpenPersonaDialog({action: '', state: false});
    };

    const confirmDelete = () => {
        dispatch(startDeletePersona(idItem))
        handleClose();
        setTimeout(() => {
            setIdItem(null)
        }, 500)
        setRecharge(true)
    }

    useEffect(() => {
        setTitle(openPersonaDialog.action)
    }, [openPersonaDialog])

    useEffect(() => {
        if (tiposDocumento.length > 0)
            setTypes([...tiposDocumento]);
    }, [tiposDocumento])

    useEffect(() => {
        if (itemEdit !== null) {
            for (let itemEditKey in itemEdit) {
                if (itemEditKey === 'perFechaNacimiento') {
                    const dateFormated = moment(itemEdit[itemEditKey], 'DD/MM/yyyy').format('yyyy-MM-dd');
                    setFieldValue(itemEditKey, dateFormated);
                }
                setFieldValue(itemEditKey, itemEdit[itemEditKey])
            }
        }
    }, [itemEdit]);

    return (
        <>
            <Dialog
                fullWidth
                maxWidth={"md"}
                open={openPersonaDialog.state}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle>
                    Datos generales: <Typography>{capitalizeLabel(title)}</Typography>
                </DialogTitle>
                <DialogContent>
                    {
                        openPersonaDialog.action !== 'borrar' ?
                            <form onSubmit={handleSubmit} className={'form_personas'}>
                                <Box sx={{mb: 2}}>
                                    <TextField
                                        name="perNombre"
                                        label="Nombre"
                                        value={values.perNombre}
                                        helperText={errors.perNombre}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        variant="outlined"
                                        fullWidth
                                        size={'small'}
                                    />
                                </Box>
                                <Box sx={{mb: 2}}>
                                    <TextField
                                        name="perApellido"
                                        label="Apellido"
                                        value={values.perApellido}
                                        helperText={errors.perApellido}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        variant="outlined"
                                        fullWidth
                                        size={'small'}
                                    />
                                </Box>

                                <Box sx={{mb: 2}}>
                                    <TextField
                                        name="perNumeroDocumento"
                                        label="Número de documento"
                                        value={values.perNumeroDocumento}
                                        helperText={errors.perNumeroDocumento}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        variant="outlined"
                                        type="number"
                                        fullWidth
                                        size={'small'}
                                    />
                                </Box>

                                <Box sx={{mb: 2}}>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-helper-label">
                                            Tipo de documento
                                        </InputLabel>
                                        <Select
                                            labelId="demo-simple-select-helper-label"
                                            name="perTipoDocumento"
                                            label="Tipo de documento"
                                            value={values.perTipoDocumento}
                                            onChange={handleChange}
                                            variant="outlined"
                                            error={!!errors.perTipoDocumento}
                                            size={'small'}
                                        >
                                            {types.map((tipo: any) => (
                                                <MenuItem key={tipo.id} value={tipo.description}>
                                                    {tipo.description}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </Box>

                                <Box sx={{mb: 2}}>
                                    <TextField
                                        name="perFechaNacimiento"
                                        label="Fecha de nacimiento"
                                        value={values.perFechaNacimiento}
                                        helperText={errors.perFechaNacimiento}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        variant="outlined"
                                        type="date"
                                        fullWidth
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        size={'small'}
                                    />
                                </Box>
                                {
                                    openPersonaDialog.action === 'crear' ?
                                        <Button sx={{height: 40}} variant="contained" type="submit" disabled={!isValid}>
                                            Enviar
                                        </Button>
                                        :
                                        <Button sx={{height: 40}} variant="contained" type="submit" disabled={!isValid}>
                                            Modificar
                                        </Button>
                                }

                            </form>
                            :
                            <Typography>
                                ¿Está seguro de que quiere eliminar el registro? Luego de esta acción no se podrán
                                recuperar los datos. </Typography>

                    }

                </DialogContent>
                <DialogActions>
                    <Button sx={{height: 40}} color={'error'} onClick={handleClose}>
                        Cancelar
                    </Button>
                    {
                        openPersonaDialog.action === 'borrar' &&
                        <Button sx={{height: 40}} onClick={confirmDelete}>
                            Confirmar
                        </Button>
                    }

                </DialogActions>
            </Dialog>
        </>
    )
}