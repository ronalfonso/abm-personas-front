import {ResponseRequest} from "../models/ResponseRequest";

export const helpFetch = () => {
    const customFetch = (url: string, options: any) => {

        const defaultHeader = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:8080'
        };

        // options.mode = 'no-cors';
        options.method = options.method || 'GET';
        options.headers = options.headers ? {...options, ...defaultHeader} : defaultHeader;

        if (options.headers.headers) {
            Object.assign(options.headers, options.headers.headers);
            delete options.headers.headers;
        }

        options.body = JSON.stringify(options.data) || false;
        delete options.data
        if (options.method === 'GET') delete options.body;

        let status: any = null, statusText: any = null, error: any = null;
        return fetch(url, options)
            .then(resp => {
                status = resp.status;
                statusText = resp.statusText.toUpperCase().replace(' ', '_');
                error = !resp.ok;
                return resp.json();
            })
            .then(data => {
                if (status >= 200 && status < 300 && data.status >= 200 && data.status < 300) {
                    const response: ResponseRequest = {
                        data,
                        status,
                        statusText,
                        error,
                        message: data.message
                    }
                    return response;
                } else {
                    if (data.status > 400) {
                        status = data.status
                        error = data.error
                        statusText = data.statusText
                    }
                    const response: ResponseRequest = {
                        data: data,
                        status: status || 0,
                        statusText: statusText || 'DICTIONARY.ERROR_OCCURRED',
                        error: error,
                        message: data.message || data.error
                    }
                    return response;
                }
            })
    }
    const get = (url: string, options = {}) => customFetch(url, options);
    const post = (url: string, options: any = {}) => {
        options.method = 'POST'
        return customFetch(url, options)
    }
    const put = (url: string, options: any = {}) => {
        options.method = 'PUT'
        return customFetch(url, options)
    }
    const del = (url: string, options: any = {}) => {
        options.method = 'DELETE'
        return customFetch(url, options)
    }

    return {
        get, post, put, del,
    };
}