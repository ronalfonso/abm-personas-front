import {combineReducers} from "@reduxjs/toolkit";
import {authSlice} from "./auth";
import {personaSlice} from "./personas/personaSlice";

const rootReducer = combineReducers({
    auth: authSlice.reducer,
    personas: personaSlice.reducer,
});

export default rootReducer;