import {Moment} from "moment";

export interface IPersona {
    id: number;
    perNombre: string;
    perApellido: string;
    perTipoDocumento: string;
    perNumeroDocumento: string;
    perFechaNacimiento: string;
}

export interface IPersonaCreate {
    perNombre: string;
    perApellido: string;
    perNumeroDocumento: string;
    perTipoDocumento: string;
    perFechaNacimiento: string | Moment | number;
}