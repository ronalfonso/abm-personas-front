import {createSlice} from "@reduxjs/toolkit";
import {IPersona} from "./interfaces/Persona";
import {ITipoDocumentos} from "./interfaces/TipoDocumentos";

const initTiposDocumento: ITipoDocumentos[] = [{
    id: 0,
    description: ''
}]

const initPersonasState: IPersona[] = [{
    id: 0,
    perNombre: '',
    perApellido: '',
    perTipoDocumento: '',
    perNumeroDocumento: '',
    perFechaNacimiento: '',
}]

const initialState = {
    personaList: initPersonasState,
    tiposDocumento: initTiposDocumento,
}

export const personaSlice = createSlice({
    name: 'Personas',
    initialState,
    reducers: {
        personas: (state, {payload}) => {
            state.personaList = payload
        },
        types: (state, {payload}) => {
            state.tiposDocumento = payload
        },
    }
})

export const { personas, types } = personaSlice.actions;