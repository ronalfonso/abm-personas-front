import {StatusEnum} from "./enum/status.enum";
import {createSlice} from "@reduxjs/toolkit";

export interface AuthState {
    token: string,
    status: string,
    user: any,
    error: any,
}

const initialState: AuthState = {
    token: '',
    status: StatusEnum.NOT_AUTHENTICATED,
    user: null,
    error: null,
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        login: ((state, {payload}) => {
            state.status = StatusEnum.AUTHENTICATED; // 'checking', 'not-authenticated', 'authenticated'
            state.token = payload.token;
            state.user = payload.user;
        }),
        logout: (state, {payload}) => {
            state.status = StatusEnum.NOT_AUTHENTICATED; // 'checking', 'not-authenticated', 'authenticated'
            state.token = '';
            state.user = null;
            state.error = payload;
        },
        checkingCredentials: (state) => {
            state.status = StatusEnum.CHECKING
        },
    }
})

export const {login, logout, checkingCredentials} = authSlice.actions;