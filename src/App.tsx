import {Wrapper} from "./layout/Wrapper/Wrapper";
import './styles/styles.scss';
import {ThemeProvider} from "@mui/material";
import {theme} from "./theme/theme";

function App() {
  return (
      <ThemeProvider theme={theme}>
        <Wrapper />
      </ThemeProvider>
  );
}

export default App;
