import {useContext, useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../../store/store";
import {GeneralContext} from "../../contexts";
import {
    Box, Button,
    Card, CardActions,
    CardContent,
    CardHeader,
    FormControl, IconButton,
    InputLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    TextField
} from "@mui/material";
import {Add, CleaningServices, Search} from "@mui/icons-material";
import {TableComponent} from "../../core/components/table/TableComponent";
import {PersonaDialog} from "../../core/components/PersonaDialog/PersonaDialog";
import {startGetPersonaList, startGetTypeList} from "../../store/personas";
import {IPersona} from "../../store/personas/interfaces/Persona";
import {ITipoDocumentos} from "../../store/personas/interfaces/TipoDocumentos";

const HomePage = () => {
    const { setOpenPersonaDialog, setRecharge, recharge } = useContext<any>(GeneralContext);
    const {personaList, tiposDocumento} = useAppSelector((state) => state.personas)
    const dispatch = useAppDispatch();
    const [typeSelected, setTypeSelected] = useState('');
    const [rows, setRows] = useState<IPersona[]>([]);
    const [types, setTypes] = useState<ITipoDocumentos[]>([])
    const [params, setParams] = useState({
        page: 0,
        size: 10,
        nombre: '',
        tipoDocumento: ''
    })

    const handleChange = (event: SelectChangeEvent) => {
        setTypeSelected(event.target.value);
        setParams({...params, tipoDocumento: event.target.value})
    };

    const handleClickOpen = () => {
        setOpenPersonaDialog({action: 'crear', state: true});
    };

    const heads = [
        {nameData: 'id', value: 'id'},
        {nameData: 'perNombre', value: 'nombre'},
        {nameData: 'perApellido', value: 'apellido'},
        {nameData: 'perNumeroDocumento', value: 'número documento'},
        {nameData: 'perTipoDocumento', value: 'tipo documento'},
        {nameData: 'perFechaNacimiento', value: 'fecha naciemiento'},
    ]

    const handleSearch = () => {
        _getPersonaList();
    }

    const handleClean = () => {
        setParams({
            page: 0, size: 10, nombre: '', tipoDocumento: ''
        })
        _getPersonaList({
            page: 0, size: 10, nombre: '', tipoDocumento: ''
        });
    }

    const _getPersonaList = (paramsArg?: any) => {
        setRecharge(true);
        dispatch(startGetPersonaList(paramsArg !== undefined ? paramsArg : params))
        setRecharge(false);
    }

    const _getTypeList = () => {
        dispatch(startGetTypeList())
    }


    useEffect(() => {
        const promises = [_getPersonaList(), _getTypeList()];
        Promise.all(promises)
    },[])

    useEffect(() => {
        if (recharge === true)
        _getPersonaList();
    }, [recharge]);

    useEffect(() => {
        setRows(personaList);
    }, [personaList])

    useEffect(() => {
        if (tiposDocumento.length > 0)
            setTypes([...tiposDocumento]);
    }, [tiposDocumento])

    return (
        <>
            <div className={'content'}>
                <Card className="filters" elevation={12}>
                    <CardHeader sx={{p: 0}} title={'Filtros'}/>
                    <CardContent>
                        <Box>
                            <TextField
                                id="outlined-required"
                                label="Nombre"
                                name={'nombre'}
                                value={params.nombre}
                                onChange={(e) => {
                                    setParams({...params, nombre: e.target.value})
                                }}
                                placeholder={'Nombre'}
                                size={'small'}
                            />

                            <FormControl sx={{ml: 10, minWidth: 210}}>
                                <InputLabel id="demo-simple-select-helper-label">Tipo documento</InputLabel>
                                <Select
                                    labelId="demo-simple-select-helper-label"
                                    value={typeSelected}
                                    label="Tipo documento"
                                    size={'small'}
                                    onChange={handleChange}
                                >
                                    {
                                        types.map((tipo) => {
                                            return (
                                                <MenuItem key={tipo.id}
                                                          value={tipo.description}>{tipo.description}</MenuItem>
                                            )
                                        })
                                    }
                                </Select>
                            </FormControl>
                        </Box>

                    </CardContent>
                    <CardActions sx={{display: 'flex', justifyContent: 'space-between'}}>
                        <Button variant={'text'}
                                color={'secondary'}
                                onClick={handleClean}
                        ><CleaningServices/>Limpiar</Button>
                        <Button variant={'contained'}
                                onClick={handleSearch}
                        ><Search/>Buscar</Button>
                    </CardActions>
                </Card>

                <Card className="table_personas" elevation={12}>
                    <CardHeader sx={{p: 0}} title={'Personas'}
                                action={
                                    <IconButton onClick={handleClickOpen}>
                                        <Add/>
                                    </IconButton>
                                }
                    />
                    <CardContent>
                        <TableComponent
                            heads={heads}
                            rows={rows}
                            canEdit={true}
                            canDelete={true}
                        />
                    </CardContent>
                </Card>
            </div>

            <PersonaDialog/>

        </>
    )
}

export default HomePage;