import {useMemo, useState} from "react";
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {Card, CardContent, IconButton} from "@mui/material";
import {AccountCircle, Visibility, VisibilityOff} from "@mui/icons-material";
import { TextField, Button } from '@mui/material';
import {StatusEnum} from "../../store/auth/enum/status.enum";
import {useAppDispatch, useAppSelector} from "../../store/store";
import {starLogin} from "../../store/auth";

const LoginPage = () => {
    const dispatch = useAppDispatch();
    const status = useAppSelector((state) => state.auth.status);
    const isCheckingAuth = useMemo(() => status === StatusEnum.CHECKING, [status]);
    const [showPassword, setShowPassword] = useState(false);
    const yupSchema = Yup.object({
        username: Yup.string().required("El nombre de usuario es obligatorio"),
        password: Yup.string().required("La contraseña es obligatoria").min(6, "La contraseña debe tener al menos 6 caracteres"),
    });

    const { values, errors, handleBlur, handleChange, handleSubmit, isValid } = useFormik({
        initialValues: {
            username: "",
            password: "",
        },
        validationSchema: yupSchema,
        onSubmit: (values) => {
            dispatch(starLogin(values))
        },
    });



    return (
        <div className={'card_container'}>
            <Card className={'card_login'} elevation={12}>
                <CardContent>
                    <div className="header_img">
                        <AccountCircle sx={{fontSize: 180}} />
                    </div>
                    <div className="content_login">
                        <form onSubmit={handleSubmit}>
                            <TextField
                                label="Nombre de usuario"
                                name="username"
                                value={values.username}
                                helperText={errors.username}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                margin="normal"
                                fullWidth
                            />
                            <TextField
                                label="Contraseña"
                                name="password"
                                type={showPassword ? "text" : "password"}
                                value={values.password}
                                helperText={errors.password}
                                onBlur={handleBlur}
                                onChange={handleChange}
                                margin="normal"
                                fullWidth
                                InputProps={{
                                    endAdornment: (
                                        <IconButton
                                            onClick={() => setShowPassword(!showPassword)}
                                            aria-label="Toggle password visibility"
                                        >
                                            {showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    ),
                                }}
                            />
                            <Button
                                type="submit"
                                variant="contained"
                                disabled={!isValid || isCheckingAuth}
                                style={{width: '100%', marginTop: '1rem'}}>
                                Iniciar sesión
                            </Button>
                        </form>
                    </div>

                </CardContent>

            </Card>
        </div>
    )
}

export default LoginPage;